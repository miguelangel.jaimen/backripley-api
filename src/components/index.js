const entitiRoutes = require('./entity');
const loginRoutes = require('./login');
module.exports = {
    entitiRoutes,
    loginRoutes
}