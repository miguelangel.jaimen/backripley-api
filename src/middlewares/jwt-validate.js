const JWT = require('jsonwebtoken');
const moment = require('moment');

const jwtValidate = (req, res, next) => {

    // Leer token
    const { jwt } = req.headers;

    // si no viene el token retornar error
    if(!jwt){
        console.error('JWT-VALIDATE: jwt no entregado');
        return res.status(401).json({
            httpCode: 401,
            msjCliente: "JWT no entregado",
            msjTecnico: "JWT no entregado",
        });
    }
    // verificar el token
    try {
        const payload = JWT.verify(jwt, process.env.JWT_SECRET);
        req.payload = payload;
        next();
    } catch (error) {
        console.error(`JWT-VALIDATTE: ${error.message}`);
        return res.status(401).json({
            httpCode: 401,
            msjCliente: "JWT inválido",
            msjTecnico: error.message
        });
    }

}

module.exports = {
    jwtValidate
}