const parametros = require("./params");
const { resultado } = require("../../helpers/result");
const { JWTGenerate } = require("../../helpers/jwt-generate");
const Login = require("../../../models/").login;
const bcrypt = require("bcryptjs");

async function signIn(req, res) {
  // informacion de la operación
  const op = "registro";
  let msjCode = "001";

  // config data
  let opResult = {};
  const salt = bcrypt.genSaltSync();
  const login = new Login(req.body);
  login.estado = 1;
  //encriptar clave
  login.clave = bcrypt.hashSync(req.body.clave, salt);

  // guardar registro
  try {
    const data = await login.save();
    try {
      opResult = await resultado(op, parametros, msjCode, data);
    } catch (err) {
      msjCode = "003";
      opResult = await resultado(op, parametros, msjCode, err);
    }
  } catch (err) {
    msjCode = "002";
    opResult = await resultado(op, parametros, msjCode, err);
  }

  // formato generico de respuesta
  return res.status(opResult.httpCode).send(opResult);
}

async function login(req, res) {
  // información de la operación
  const op = "login";
  let msjCode = "001";

  // config data
  let opResult = {};

  // realizar ingreso
  try {
    const { correo, clave, rut, dv, nombre } = req.body;
    const user = await Login.findOne({ where: { correo: correo } });
    const compare = bcrypt.compareSync(clave, user.clave);
    if (compare) {
      // generar JWT
      const { token } = await JWTGenerate({ rut, dv, correo, nombre });
      data = { correo, rut, dv, nombre, token };
    } else {
      msjCode = "002";
      data = {};
    }
    try {
      opResult = await resultado(op, parametros, msjCode, data);
    } catch (err) {
      msjCode = "003";
      opResult = await resultado(op, parametros, msjCode, err);
    }
  } catch (err) {
    msjCode = "002";
    opResult = await resultado(op, parametros, msjCode, err);
  }
  return res.status(opResult.httpCode).send(opResult);
}

async function list(req, res) {
  // información de la operación
  const op = "list";
  let msjCode = "001";
  let opResult = {};

  // listar usuarios
  try {
    const data = await Login.findAll();
    if (!data.length) msjCode = "002";

    try {
      opResult = await resultado(op, parametros, msjCode, data);
    } catch (err) {
      msjCode = "003";
      opResult = await resultado(op, parametros, msjCode, err);
    }
  } catch (err) {
    msjCode = "004";
    opResult = await resultado(op, parametros, msjCode, err);
  }
  return res.status(opResult.httpCode).send(opResult);
}

module.exports = {
  signIn,
  login,
  list,
};
