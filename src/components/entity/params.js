module.exports = {
    nombreArtefacto: 'entity',
    token: true,
    operaciones:{
      'list':{
        nombre:'listar',
        mensajes: {
          '001': {
              httpCode: 200,
              status: true,
              msjCliente: 'listar ok',
              msjTecnico: 'listar ok',
          },
          '002': {
              httpCode: 400,
              status: true,
              msjCliente: 'No existen datos para listar',
              msjTecnico: 'No existen datos para listar',
          },
          '003': {
              httpCode: 400,
              status: false,
              msjCliente: 'Error al listar',
              msjTecnico: 'Error en el servidor',
          },
          '004': {
              httpCode: 500,
              status: false,
              msjCliente: 'Error al listar',
              msjTecnico: 'Error en el servidor',
          }
        },
      },
      'obtenerJwt':{
        nombre:'obtenerJwt',
        mensajes: {
          '001': {
              httpCode: 200,
              status: true,
              msjCliente: 'obtenerJwt ok',
              msjTecnico: 'obtenerJwt ok',
          },
          '002': {
              httpCode: 400,
              status: false,
              msjCliente: 'Error al obtenerJwt',
              msjTecnico: 'Error al obtenerJwt',
          },
          '003': {
              httpCode: 500,
              status: false,
              msjCliente: 'Error al obtenerJwt',
              msjTecnico: 'Error en el servidor',
          }
        },
      },
    }
};