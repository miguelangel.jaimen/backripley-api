'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Login extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Login.init({
    id_login: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    rut: {
      allowNull:false,
      type:DataTypes.INTEGER,
      unique: true
    },
    dv: {
      allowNull:false,
      type:DataTypes.STRING(1)
    },
    nombre: {
      allowNull:false,
      type:DataTypes.STRING
    },
    correo: {
      allowNull:false,
      type:DataTypes.STRING
    },
    clave: {
      allowNull:false,
      type:DataTypes.STRING
    },
    estado: {
      allowNull:false,
      type:DataTypes.BOOLEAN
    }
  }, {
    sequelize,
    modelName: 'login',
  });
  return Login;
};