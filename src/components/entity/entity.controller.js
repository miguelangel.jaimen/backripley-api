const parametros = require('./params');
const mock = require('./mock');
const { resultado } = require('../../helpers/result');
const { JWTGenerate } = require('../../helpers/jwt-generate');
const { default: axios } = require('axios');

async function list(req, res) {
    const op = 'list';
    let msjCode = '001';
    let opResult ={};

    try {
        
    // Obtener informacion
    // const data = await EntityService.List();
      const  {data} = await axios.get('https://jsonplaceholder.typicode.com/posts');
      console.log(data)
      // const  data = mock.list;
      // const data = [];

      if (!data.length) msjCode = '002';

      try {
        opResult = await resultado(op, parametros, msjCode, data);
      } catch (err) {
        msjCode = '003';
        opResult = await resultado(op, parametros, msjCode, err);
      }
    } catch (err) {
      msjCode = '004';
      opResult = await resultado(op, parametros, msjCode, err);
    }
    return res.status(opResult.httpCode).send(opResult);
  }

 async function getjwt(req, res){
   const op = 'obtenerJwt';
   msjCode = '001';

   try {
     const data = await JWTGenerate({nombre:"miguelangel", edad:34, soltero:false});
  
      if(!data) {
        msjCode = '002'
        let opResult = await resultado(op, parametros, msjCode, data)
        return res.status(opResult.httpCode).send(opResult);
      }
      try {
        let opResult = await resultado(op, parametros, msjCode, data)
        return res.status(opResult.httpCode).send(opResult);
      } catch (error) {
        msjCode = '002'
        console.error(error)
        let opResult = await resultado(op, parametros, msjCode, data)
        return res.status(opResult.httpCode).send(opResult);
      }
    } catch (error) {
     msjCode = '003'
    console.error(error)
    let opResult = await resultado(op, parametros, msjCode, data)
    return res.status(opResult.httpCode).send(opResult);
   }
 }

  module.exports = {
      list,
      getjwt
  }