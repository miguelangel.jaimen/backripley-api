const jwt = require('jsonwebtoken');

const JWTGenerate = (payload) => {

    return new Promise((resolve, reject) => {
        jwt.sign(payload, process.env.JWT_SECRET, {
            expiresIn: '24h'
        }, (err, token)=>{
            if(err){
                console.log(err);
                reject('No se pudo generar token');
            }else{
                resolve({token});
            }
        })
    });
};

module.exports = {
    JWTGenerate
}