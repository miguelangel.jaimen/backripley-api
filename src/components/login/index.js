const { signIn, login, list } = require("./login.controller");
const { Router } = require("express");
const { body } = require("express-validator");
const { validarCampos } = require("../../middlewares/validar-campos");
const { jwtValidate } = require("../../middlewares/jwt-validate");

router = Router();

// registro
router.post(
  "/registro",
  [body("nombre", "El nombre es obligatorio").isString(), validarCampos],
  signIn
);

// ingreso
router.post("/ingreso", login);

// listar
router.get("/list", jwtValidate, list);

module.exports = router;
