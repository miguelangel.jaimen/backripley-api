module.exports = {
    nombreArtefacto: 'login',
    token: true,
    operaciones:{
      'registro':{
        nombre:'registro',
        mensajes: {
          '001': {
              httpCode: 200,
              status: true,
              msjCliente: 'registro ok',
              msjTecnico: 'registro ok',
          },
          '002': {
              httpCode: 400,
              status: true,
              msjCliente: 'Registro no realizado',
              msjTecnico: 'Registro no realizado',
          },
        },
      },
      'login':{
        nombre:'ingreso',
        mensajes: {
          '001': {
              httpCode: 200,
              status: true,
              msjCliente: 'Ingreso ok',
              msjTecnico: 'Ingreso ok',
          },
          '002': {
              httpCode: 400,
              status: true,
              msjCliente: 'Ingreso no realizado',
              msjTecnico: 'Ingreso no realizado',
          },
        },
      },
      'list':{
        nombre:'listar',
        mensajes: {
          '001': {
              httpCode: 200,
              status: true,
              msjCliente: 'listar ok',
              msjTecnico: 'listar ok',
          },
          '002': {
              httpCode: 400,
              status: true,
              msjCliente: 'No existen datos para listar',
              msjTecnico: 'No existen datos para listar',
          },
          '003': {
              httpCode: 400,
              status: false,
              msjCliente: 'Error al listar',
              msjTecnico: 'Error en el servidor',
          },
          '004': {
              httpCode: 500,
              status: false,
              msjCliente: 'Error al listar',
              msjTecnico: 'Error en el servidor',
          }
        },
      },
    }
};