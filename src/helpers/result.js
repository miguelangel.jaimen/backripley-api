'use strict'

// generalización de respuestas de una operacion
exports.resultado = function(op, params, msjCod, data) {
    return new Promise((resolve, reject)=>{
        data = data || {};
        data = data.original ? {error: data.original.sqlMessage} : data;
        let artefacto = params.nombreArtefacto.toUpperCase();
        let operacion = params.operaciones[op];
        let respuesta = Object.assign(operacion.mensajes[msjCod], {data});
        // falta agregar registro en tabla de logs
        if(!data.error){
            console.log(operacion.mensajes[msjCod].status);
            operacion.mensajes[msjCod].status?
            console.info(`${artefacto}-${op.toUpperCase()}: ${operacion.mensajes[msjCod].msjTecnico}`):
            console.error(`${artefacto}-${op.toUpperCase()}: ${operacion.mensajes[msjCod].msjTecnico}`);
            resolve(respuesta);
        }else{
            console.error(`${artefacto}-${op.toUpperCase()}: ${operacion.mensajes[msjCod].msjTecnico}`);
            respuesta.error = data.error;
            delete respuesta.data;
            resolve(respuesta);
        }
    });
};