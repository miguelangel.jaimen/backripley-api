const {list, getjwt} = require('./entity.controller');
const { Router } = require('express');
const { body } = require('express-validator');
const { validarCampos } = require('../../middlewares/validar-campos');
const { jwtValidate } = require('../../middlewares/jwt-validate');

router = Router();

router.get('', jwtValidate, list);

router.get('/jwt',[
    body('nombre','El nombre es obligatorio').matches(/\w/g),
    validarCampos

], getjwt);

router.post('/crear',
[
    body('nombre','El nombre es obligatorio').matches(/bla/g),
    validarCampos
]
, list);

module.exports = router;