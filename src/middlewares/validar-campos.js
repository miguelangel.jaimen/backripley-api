const { response:res } = require('express');
const { validationResult } = require('express-validator')

const validarCampos = (req, res, next ) => {
    const errors = validationResult( req );

    if ( !errors.isEmpty() ) {
        console.error( `ERROR-REQ ( ${req.originalUrl} ): ${errors.array().map(a => a.param)}`);
        return res.status(400).json({
            httpCode: 400,
            msjCliente: "Error en la consulta",
            msjTecnico: "Error de validacion req",
            errores: errors.mapped()
        });
    }

    next();
}

module.exports = {
    validarCampos
}
