require('dotenv').config();
require('console-info');
require('console-error');
require('./models');

// const { dbConnection } = require('./database/config');
const { entitiRoutes, loginRoutes} = require('./src/components/');

const express =require('express');
const cors = require('cors');


// Declaracion
const app = express();

//CORS
app.use(cors());

// Lectura y parseo del body
app.use( express.json() );

//DB CONECCION                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
//dbConnection();

// rutas
app.use('/entity', entitiRoutes);
app.use('/usuarios/', loginRoutes);

app.listen(process.env.PORT, () => {
 console.log('servidor corriendo en puerto', process.env.PORT );
});
